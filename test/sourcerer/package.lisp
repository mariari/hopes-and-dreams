#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#
(cl:in-package #:common-lisp-user)

(defpackage #:hopes-and-dreams.test.sourcerer
  (:use #:cl #:parachute)
  (:local-nicknames
   (#:us #:utena-symbols)
   (#:s #:hopes-and-dreams.sourcerer)
   (#:h #:hopes-and-dreams)
   (#:i #:hopes-and-dreams.utena.interaction)))
