(cl:in-package #:asdf-user)

(defsystem #:hopes-and-dreams.object
  :author "Gnuxie <gnuxie@applied-langua.ge>"
  :serial t
  :depends-on
  ("alexandria"
   "hopes-and-dreams.protocol")
  :components
  ((:file "package")
   (:file "slot-description")
   (:file "standard-class")))
