#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

;;;; basically there has to be a new compilation unit from when
;;;; we defined all the instructions otherwise this dispatch macro
;;;; will expand before the define-instruction forms have been
;;;; evaluated.

(in-package #:hopes-and-dreams)

(define-instruction-dispatch)

(defun dispatch-loop (machine)
  (tagbody
   :start
     (when (null (activated-method machine))
       ;; we reached the bottom of the guest stack, congratulations.
       (return-from dispatch-loop :bottom))
     (loop
       while (< (instruction-pointer (activated-method machine))
                (length (method-code (activated-method machine))))
       for bytecode = (aref (method-code (activated-method machine))
                            (instruction-pointer (activated-method machine)))
       for result = (dispatch machine
                              (high-nibble  bytecode)
                              (low-nibble   bytecode)
                              (operand-byte bytecode))
       if (null result) do (incf (instruction-pointer (activated-method machine)))
         else do (setf (activated-method machine)
                       result)
                 and do
                   (go :start))
     ;; once we exit the loop
     (setf (activated-method machine)
           (escape-record (activated-method machine)))
     (unless (null (activated-method machine))
       (incf (instruction-pointer (activated-method machine))))
     (go :start)))
