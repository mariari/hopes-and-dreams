#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(declaim (inline make-high-nibble make-low-nibble make-operand-byte))
(defun make-high-nibble (opcode-part)
  (ash opcode-part 12))
(defun make-low-nibble (nibble)
  (ash nibble 8))
(defun make-operand-byte (operand-byte)
  operand-byte)

(define-instruction self (0 0)
  (push-data-stack (activated-self machine) machine)
  nil)

(defun make-self-instruction ()
  0)

(define-instruction literal (1 index-1 index-2)
  (let ((index (+ (ash index-1 4) index-2)))
    (push-data-stack (find-literal index
                                   (activated-method machine))
                     machine))
  nil)

(defun make-literal-instruction (index)
  (+ (make-high-nibble 1)
     index))

(define-instruction constant (2 index-1 index-2)
  (let ((index (+ (ash index-1 4) index-2)))
    (push-data-stack (find-constant machine index) machine))
  nil)

(defun make-constant-instruction (index)
  (+ (make-high-nibble 2)
     index))

(defun %send (receiver machine selector argument-count)
  ;; currently we don't use argument-count because i think we
  ;; leave it up to the evaluate-message method to pop arguments.
  ;; So we should pass it through to them or pass the arguments
  ;; through as a list. (Passing it through to them is better).
  (declare (ignore argument-count))
  (multiple-value-bind (slot object) (lookup-message receiver selector)
    (assert (not (null slot)))
    (evaluate-message slot object receiver machine)))

(define-instruction send (3 0 argument-count)
  (let ((selector (pop-data-stack machine))
        (reciever (pop-data-stack machine)))
    (%send reciever machine selector argument-count)))

(defun make-send-instruction (argument-count)
  (check-type argument-count (unsigned-byte 8))
  (+ (make-high-nibble 3)
     argument-count))

(define-instruction implicit-self-send (3 1 argument-count)
  (let ((selector (pop-data-stack machine)))
    (%send (activated-method machine)
           machine selector argument-count)))

(defun make-implicit-self-send-instruction (argument-count)
  (check-type argument-count (unsigned-byte 8))
  (+ (make-high-nibble 3)
     (make-low-nibble 1)
     argument-count))

;;; we will clear apply prefix as we read it.
(define-instruction apply-prefix (4 mode)
  (setf (apply-prefix machine) mode)
  nil)
