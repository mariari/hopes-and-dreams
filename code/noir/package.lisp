#|
    Copyright (C) 2022 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:hopes-and-dreams.noir
  (:use #:cl)
  (:export
   #:uir
   #:abstract-send
   #:selector-part
   #:argument-parts
   #:explicit-argument-count

   #:send
   #:make-send
   #:implicit-self-send
   #:make-implicit-self-send

   #:drop
   #:make-drop

   #:self
   #:make-self

   #:abstract-constant
   #:value
   #:designator

   #:literal
   #:make-literal
   #:constant))
