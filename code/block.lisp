#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(defgeneric create-block (block-template machine))
(defclass block-template ()
  ((%block-method :initarg :block-method
                  :reader block-method)))

(defun make-block-template (block-method)
  (make-instance 'block-template :block-method block-method))
