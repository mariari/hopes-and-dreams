#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.sourcerer)

(defun transform-expression (expression)
  (check-type expression cst:cst)
  (cond
    ((cst:atom expression)
     (list
      (noir:make-implicit-self-send
       (noir:make-literal expression))))

    ((cst:consp expression)
     (case (first (cst:listify expression))
       (us:send
           (cst:db source ((selector receiver) . arguments)
               (cst:rest expression)
             (list*
              (apply
               #'noir:make-send
               (noir:make-literal selector)
               (transform-expression receiver)
               (loop for argument in arguments
                     collect (transform-expression argument))))))

       (us:quote (list (noir:make-literal (cst:second expression))))

       (us:block
           (list
            (noir:make-literal
             (h:make-block-template
              (transform-method (cst:second expression) (cst:nthrest 2 expression))))))

       (t
        (cst:db source
            (selector . arguments) expression
          (list
           (noir:make-implicit-self-send
            (first (transform-expression selector))
            (loop for sub-expression in (cst:listify arguments)
                  appending (transform-expression sub-expression))))))))))

(defun transform-method (argument-selectors body)
  (let* ((method-noir
           (loop for expression in (cst:listify body)
                 appending (transform-expression expression)))
         (method-arguments
           (loop
             with arguments = (make-array 3 :adjustable t :fill-pointer 0)
             for selector in (cst:listify argument-selectors)
             do (hopes-and-dreams:make-argument-slot selector arguments)
             finally (return arguments))))

    (let ((method
            (make-instance
             'hopes-and-dreams:method
             :method-uir method-noir
             :method-arguments method-arguments)))
      (loop for argument across (h::method-arguments method)
            do (h::add-slot method argument)
            finally (return method)))))
