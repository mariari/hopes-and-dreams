#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.sourcerer)

(define-method-macro us:let (cst-form)
  (destructuring-bind (let-symbol bindings-assoc &rest body)
      (cst:listify cst-form)
    (declare (ignore let-symbol))
    (cst:quasiquote
     (cst:source cst-form)
     (us:send
         (us:value
          (us:block ((cst:unquote-splicing (mapcar #'cst:first (cst:listify bindings-assoc))))
            (cst:unquote-splicing body)))
         (cst:unquote-splicing (mapcar #'cst:second (cst:listify bindings-assoc)))))))
