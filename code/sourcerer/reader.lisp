#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams.sourcerer)

(defvar *client*)

(defclass sourcerer-reader () ())

(defun sourcerer-read-from-string (input)
  (let ((*client* (make-instance 'sourcerer-reader)))
    (read-from-string input)))

(defun sourcerer-read-from-input (input)
  (let ((*client* (make-instance 'sourcerer-reader)))
    (read-from-file input)))

(defmethod check-symbol-token ((client sourcerer-reader) input-stream
                               token escape-ranges position-package-marker-1
                               position-package-marker-2))
