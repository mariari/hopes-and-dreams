#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:hopes-and-dreams)

(defparameter *instructions* '())

(defclass instruction ()
  ((%name
    :initarg :name
    :reader instruction-name)
   (%high-nibble
    :initarg :high-nibble
    :reader instruction-high-nibble
    :type (unsigned-byte 4))
   (%low-nibble
    :initarg :low-nibble
    :reader instruction-low-nibble
    :type (or (unsigned-byte 4) symbol))
   (%operand-byte
    :initarg :operand-byte
    :reader instruction-operand-byte
    :type (or nil symbol))
   (%byte-spec
    :initarg :byte-spec
    :reader instruction-byte-spec
    :type list)
   (%body
    :initarg :body
    :reader instruction-body
    :type list)))

(defmethod print-object ((object instruction) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~a" (instruction-name object))))

(defun make-instruction (&rest args)
  (push (apply #'make-instance 'instruction args) *instructions*))

(defmacro define-instruction
    (name (high-nibble low-nibble &optional operand-byte) &body body)
  (labels ((make-argument-list ()
             (loop for argument in (list high-nibble low-nibble operand-byte)
                   unless (or (null argument)
                              (numberp argument))
                     collect argument))
           (make-inline-body ()
             `(lambda ,(make-argument-list)
                ,@body)))
    `(progn
       ;; we do this so it gets linted by SBCL in place.
       (let ((machine '()))
         (declare (ignorable machine))
         ,(make-inline-body))
       (make-instruction :name ',name
                         :high-nibble ',high-nibble
                         :low-nibble ',low-nibble
                         :operand-byte ',operand-byte
                         :body ',(make-inline-body)
                         :byte-spec
                         '(,high-nibble ,low-nibble
                                  . ,(and operand-byte
                                         (list operand-byte)))))))

(defun make-opcode-instruction-alist (instructions)
  (macrolet
      ((split-by-opcode (opcode-accessor instructions assoc-place)
         `(dolist (instruction ,instructions)
            (push instruction
                  (alexandria:assoc-value ,assoc-place
                                          (,opcode-accessor instruction))))))
    ;; group by high opecode then low-opcode
    ;; then split.
    (let ((instructions-by-high-nibble '()))
      (split-by-opcode
       instruction-high-nibble instructions instructions-by-high-nibble)
      (dolist (next-instructions instructions-by-high-nibble)
        (let ((instructions-by-low-nibble '()))
          (split-by-opcode instruction-low-nibble (cdr next-instructions)
                           instructions-by-low-nibble)
          (setf (cdr next-instructions) instructions-by-low-nibble)))
      instructions-by-high-nibble)))

(defun generate-dispatch (instructions)
  (flet ((make-instruction-body-call (instruction)
           (let* ((argument-list (instruction-byte-spec instruction))
                  (template-list '(high-nibble low-nibble operand-byte))
                  (callee-list
                    (loop for argument in argument-list
                          for i from 0 below 3
                          if (symbolp argument)
                            collect (nth i template-list))))
             `(,(instruction-body instruction)
               ,@ callee-list))))
    (let ((opcode-instruction-alist
            (make-opcode-instruction-alist instructions)))
      `(defun dispatch (machine high-nibble low-nibble &optional operand-byte)
         (case high-nibble
           ,@ (loop for (high-op . high-candidates)
                      in opcode-instruction-alist
                    if (= 1 (length high-candidates))
                      collect `(,high-op
                                ,(make-instruction-body-call
                                  (second (first high-candidates))))
                    else collect
                    `(,high-op
                      (case low-nibble
                        ,@ (loop for (low-op low-candidate)
                                   in high-candidates
                                 collect `(,low-op
                                           ,(make-instruction-body-call
                                             low-candidate)))))))))))

(defmacro define-instruction-dispatch ()
  (generate-dispatch *instructions*))
